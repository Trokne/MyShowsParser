﻿using MyShowsParser.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MyShowsParser
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnGetResult_Click(object sender, RoutedEventArgs e)
        {
            if (udSearchFrom.Value > (int)udSearchTo.Value)
            {
                MessageBox.Show("Кажется, \"от\" и \"до\" перепутаны местами. Попробуйте ещё раз?", "Ошибка!", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            tbResult.Text = (new MyShowsResources(tbUri.Text,
                (int)udSearchFrom.Value, (int)udSearchTo.Value)).GetResult();

        }
    }
}