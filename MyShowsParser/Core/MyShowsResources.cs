﻿using CsQuery;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyShowsParser.Core
{
    public class MyShowsResources
    {
        private string _mainUri;
        private int _pageFrom;
        private int _pageTo;
        private int _curPage;

        public MyShowsResources(string mainUri, int pageFrom, int pageTo)
        {
            _mainUri = mainUri[mainUri.Length - 1] == '/' ? mainUri : mainUri + @"/";
            _pageFrom = pageFrom;
            _curPage = pageFrom - 1;
            _pageTo = pageTo;
        }

        private string GetNextUrl()
        {
            if (_curPage + 1 > _pageTo)
                return null;
            string result = $"{_mainUri}?page={_curPage}";
            _curPage++;
            return result;
        }

        public string GetResult()
        {
            string url;
            int i, pauseCount, onAirCount, deadCount;
            i = pauseCount = onAirCount = deadCount = 0;
            string result = $"Список сериалов с {_pageFrom} по {_pageTo} страницы:\n";
            while ((url = GetNextUrl()) != null)
            {
                CQ cq = CQ.CreateFromUrl(url);
                foreach (var obj in cq.Find("table.catalogTable tbody tr td"))
                {
                    if (obj.ChildNodes.Count < 3) continue;
                    string text = obj.ChildNodes[0].FirstChild?.ToString();
                    if (!String.IsNullOrWhiteSpace(text))
                    {
                        i++;
                        string status = obj.ChildNodes[2]?.GetAttribute("title");
                        result += $"{i}. {text}, статус: {status}\n";
                        if (status == "TBD") pauseCount++;
                        else if (status == "Сериал продолжается") onAirCount++;
                        else if (status == "Отменен/закончен") deadCount++;
                    }
                }        
            }
            string stats = $"Итого сериалов: {i}, из которых \"на паузе\": " +
                $"{pauseCount}, идут: {onAirCount}, отмененных/законченных: {deadCount}.\n\n";
            return stats + result; ;
        }
    }
}
